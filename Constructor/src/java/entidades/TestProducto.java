package entidades;

public class TestProducto {
    public static void main(String[] args) {
        System.out.println("Hola");
        Producto miProducto = new Producto("Licuadora");
        System.out.println(miProducto);
        
        Producto miProducto2 = new Producto("Multiprocesadora");
        System.out.println(miProducto2);
        
        Producto miProducto3 = new Producto("Telefono", 700);
        miProducto3.setNombre("Telefono Samsung");
        System.out.println(miProducto3);
        
        Producto miProducto4 = new Producto("");
        System.out.println(miProducto4);
        
        Producto miProducto5 = new Producto("Heladera", 100000, 3, 20000);
        System.out.println(miProducto5);
        
        miProducto.setPrecio(10000);
        System.out.println(miProducto);
        System.out.println("Chau");
    }
}
