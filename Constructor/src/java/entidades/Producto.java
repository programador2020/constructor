package entidades;

public class Producto {
    private String nombre;
    private int precio;
    private int stock;
    private int impuestos;
    
    public Producto(String pNombre) {
        //this.nombre = pNombre;
        setNombre(pNombre);
        validarTodo();
    }
    
    public Producto(String pNombre, int pPrecio){
        //this.nombre = pNombre;
        this.precio = pPrecio;
        validarNombre(pNombre);
        validarPrecio();
        validarStock();
    }

    public Producto(String nombre, int precio, int stock, int impuestos) {
        //this.nombre = nombre;
        this.precio = precio;
        this.stock = stock;
        this.impuestos = impuestos;
        validarNombre(nombre);
    }
    
    
    
    void validarNombre(String pNombre){
        if (pNombre == "") {
            System.out.println("El nombre no puede esta vacio");
        }else{
            this.nombre = pNombre;
        }
    }
    
    void validarPrecio(){
    }
    void validarStock(){
    }
    
    void validarTodo() {
        validarNombre(nombre);
        validarPrecio();
        validarStock();
    }
    
    void reservar() {
    }
    
    void incrementarStock() {
    }

    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", precio=" + precio + ", stock=" + stock + ", impuestos=" + impuestos + '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        validarNombre(nombre);
        //this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(int impuestos) {
        this.impuestos = impuestos;
    }
    
}
